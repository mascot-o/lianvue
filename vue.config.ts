module.exports = {
    devServer: {
        proxy: {
            '/laravel/public/api': {
                target: 'http://[::1]:80', // 更新为 [::1]:80
                changeOrigin: true,
                pathRewrite: {
                    '^/laravel/public/api': '/laravel/public/api',
                },
            },
        },
    },
};