// router/index.ts
import { createRouter, createWebHashHistory, type RouteRecordRaw } from 'vue-router'
import { Files, User, EditPen, Promotion, WarningFilled, QuestionFilled } from '@element-plus/icons-vue'

export const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "403" */ '../views/HomeView.vue'),
    children: [
      {
        path: '/about',
        name: 'About',
        meta: {
          title: '基本信息',
          icon: User
        },
        component: () => import('../views/AboutView.vue')
      },
      {
        path: '/taskadd',
        name: 'taskadd',
        meta: {
          title: '总任务管理',
          icon: EditPen,
        },
        component: () => import('@/views/task_add.vue')
      },
      {
        path: '/task',
        name: 'tadaycontent',
        meta: {
          title: '任务完成详情',
          icon: Files,
        },
        component: () => import('@/views/today_content.vue')
      },
      {
        path: '/problemencountered',
        name: 'problemencountered',
        meta: {
          title: '遇到的问题',
          icon: WarningFilled,
        },
        component: () => import('@/views/problem_encountered.vue')
      },
      {
        path: '/Issuedetails',
        name: 'Issuedetails',
        meta: {
          title: '问题详情',
          icon: QuestionFilled,
        },
        component: () => import('@/views/Issue_details.vue')
      },
      {
        path: '/knowledgepoint',
        name: 'knowledgepoint',
        meta: {
          title: '知识点管理',
          icon: Promotion,
        },
        component: () => import('@/views/knowledge_point.vue')
      },
    ]
  },
  {
    path: '/login',
    name: 'MyLogin',
    meta: {
      title: '登录',
    },
    component: () => import('../views/myLogin.vue')
  }

]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  document.title = `${to.meta.title} | App`
  const role = localStorage.getItem('token')

  if (!role && to.path !== '/login') {
    next('/login')
  } else if (role && to.path === '/login') {
    next({ name: 'Home' }) // 跳转到 HomeView 组件
  } else if (to.name === 'Home') {
    next({ name: 'About' }) // Redirect to the 'About' page
  } else {
    next()
  }
})
export default router