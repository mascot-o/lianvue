import axios from "axios";

const api = axios.create({
    baseURL: 'http://localhost/laravel/public/api',
});

// 添加请求拦截器
api.interceptors.request.use(
    function (config) {
        const token = localStorage.getItem('token');

        if (token) {
            config.headers['Authorization'] = `Bearer ${token}`; // 设置请求头的 Authorization
        }

        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);

export function userlist() {
    return api.post('/userlist');
}

export function changeuser(data: any) {
    return api.post('/changeuser', { data });
}

export function changeuserpassword(data: any) {
    return api.post('/changeuserpassword', { data });
}