import axios from "axios";

const api = axios.create({
    baseURL: 'http://localhost/laravel/public/api',
});

api.interceptors.request.use(
    function (config) {
        const token = localStorage.getItem('token');

        if (token) {
            config.headers['Authorization'] = `Bearer ${token}`;
        }

        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);

export function addquestion(data: any) {
    return api.post('/addquestion', { data });
}

export function questlist(params: any) {
    return api.get('/questlist', { params });
}

export function editquestion(data: any) {
    return api.post('/editquestion', { data });
}

export function completequestion(data: any) {
    return api.post('/completequestion', { data });
}

export function deletecquestion(data: any) {
    return api.post('/deletecquestion', { data });
}

