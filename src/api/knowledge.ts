import axios from "axios";

const api = axios.create({
    baseURL: 'http://localhost/laravel/public/api',
});

api.interceptors.request.use(
    function (config) {
        const token = localStorage.getItem('token');

        if (token) {
            config.headers['Authorization'] = `Bearer ${token}`;
        }

        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);

export function knowledgelist(params: any) {
    return api.get('/knowledgelist', { params });
}

export function addknowledge(data: any) {
    return api.post('/addknowledge', { data });
}

export function deletecknowledge(data: any) {
    return api.post('/deletecknowledge', { data });
}
