import axios from "axios";

const api = axios.create({
    baseURL: 'http://localhost/laravel/public/api',
});

api.interceptors.request.use(
    function (config) {
        const token = localStorage.getItem('token');

        if (token) {
            config.headers['Authorization'] = `Bearer ${token}`;
        }

        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);

export function contentlist(params: any) {
    return api.get('/contentlist', { params });
}

export function changecontent(data: any) {
    return api.post('/changecontent', { data });
}

export function addcontent(data: any) {
    return api.post('/addcontent', { data });
}
export function deleteccontent(data: any) {
    return api.post('/deleteccontent', { data });
}
export function completetask(data: any) {
    return api.post('/completetask', { data });
}
