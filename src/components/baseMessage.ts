import { ElMessage } from 'element-plus';

const baseMessage = (message: string, type = 'info', duration = 3000) => {
    const params = {
        message: message,
        type: type,
        duration: duration,
        showClose: true
    };
    (ElMessage as any)(params);
};

export default baseMessage;